import argparse
from PIL import Image


def create_png(field_size, template, third_height, fn):
    FIELD_SIZE = int(field_size)
    THIRD_HEIGHT = int(third_height)
    back = Image.open(template)
    TEMPLATE_HEIGHT = back.height
    TEMPLATE_WIDTH = back.width

    x = TEMPLATE_WIDTH * (FIELD_SIZE+1)
    y = (TEMPLATE_HEIGHT * (FIELD_SIZE+1)) - (FIELD_SIZE*THIRD_HEIGHT)
    img = Image.new("RGBA", (x, y))

    SIDE_LENGTH = (FIELD_SIZE//2)
    pos_y = 0
    row_length = SIDE_LENGTH
    for row in range(FIELD_SIZE+1):
        pos_x = (img.width//2) - (TEMPLATE_WIDTH*row_length//2) - (TEMPLATE_WIDTH//2)
        for cell in range(row_length+1):
            img.paste(back, (pos_x, pos_y), back)
            pos_x += TEMPLATE_WIDTH
        if row < (FIELD_SIZE+1)//2:
            row_length += 1
        else:
            row_length -= 1
        pos_y += (TEMPLATE_HEIGHT - THIRD_HEIGHT)
    img.save("{}.png".format(fn))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Hexagonal field image generator")
    parser.add_argument("template",
                        action="store",
                        help="Cropped PNG template of hexagonal block filename")
    parser.add_argument("-t",
                        action="store",
                        dest="third_height",
                        required=True,
                        help="Height of sloped template part, in pixels")
    parser.add_argument("-s",
                        action="store",
                        dest="field_size",
                        default=10,
                        help="Size of field, counted as result max blocks count - 1. Defaults to 10")
    parser.add_argument("-o",
                        action="store",
                        dest="output",
                        default="output",
                        help="Output PNG file name. Defaults to \"output\"")
    args = parser.parse_args()
    create_png(args.field_size, args.template, args.third_height, args.output)
