CLI use-ready demo of hexagonal field image generating tool. See `this article <https://blog.baka.solutions/hexagonal-grid-generation/>`_ for the detail explanation.
